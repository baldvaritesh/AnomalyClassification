#!/usr/bin/env python3

'''
Creating the Constants which can be used within a directory down
in the src folder
'''
CONSTANTS =  {'ORIGINALMANDI' : '../../data/original/wholesaleoniondata.csv',
              'ORIGINALRETAIL': '../../data/original/retailoniondata.csv',
              'MANDIPRICES'   : '../../data/whitened/mandiPrices.csv',
              'MANDIARRIVALS' : '../../data/whitened/mandiArrivals.csv',
              'RETAILPRICES'  : '../../data/whitened/centresPrices.csv',
              'CENTRESID'     : [10, 16, 40, 44, 50],
              'CENTRENAMES'   : ['BHUBANESHWAR', 'DELHI', 'LUCKNOW', 'MUMBAI', 'PATNA'],
              'MANDIIDS'      : [[182, 174, 194],
                                 [281, 404, 351, 312, 165, 70, 293, 164, 407, 166],
                                 [545, 323, 405, 584, 278, 288],
                                 [156, 427],
                                 [279, 376]],
              'DAYS'          : 3461,
              'STARTDATE'     : '2006-01-01',
              'ENDDATE'       : '2015-06-23'}

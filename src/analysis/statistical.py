from datetime import datetime
import statsmodels.tsa.stattools as st
from src.reading.reading import retailP, mandiP, mandiA, retailPM, mandiPM, mandiAM
import matplotlib.pyplot as plt
from src.constants import CONSTANTS

'''
Labels to create better plots
'''
yrs = (CONSTANTS['DAYS'] / 365) + 2
labels = [str(start + i) for i in xrange(0, yrs)]
intervals = [i*365 for i in xrange(0 , yrs)]

def plotCorrelation(series1, series2):
  '''
  Plot the cross correlation function between two time series
  '''
  ccf = st.ccf(series1, series2)
  plt.plot(ccf, label='Cross Correlation Function')
  plt.xticks(intervals, labels)
  plt.legend(loc='best')
  plt.show()

def plotAutoCorrelation(series):
  '''
  Plot the auto correlation function for a time series
  '''
  acf = st.acf(series)
  plt.plot(ccf, label='Auto Correlation Function')
  plt.xticks(intervals, labels)
  plt.legend(loc='best')
  plt.show()

def stationarityTest(series):
  print(st.adfuller(series))

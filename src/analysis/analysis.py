from datetime import datetime
import pandas as pd
import numpy as np
from src.constants import CONSTANTS
from src.reading.reading import retailP, mandiP, mandiA, retailPM, mandiPM, mandiAM
import matplotlib.pyplot as plt

def givePeriod(anom, idx):
  '''
  Anomaly Time Period from the final Value
  '''
  start = datetime.strptime(anomalies[0][idx-1], '%d/%m/%Y')
  end = datetime.strptime(anomalies[1][idx-1], ' %d/%m/%Y')
  s = datetime.strftime(start, '%Y-%m-%d')
  e = datetime.strftime(end, '%Y-%m-%d')
  return (s, e)

def Normalise(arr):
  '''
  Normalise each sample
  '''
  m = arr.mean()
  am = arr.min()
  aM = arr.max()
  arr -= m
  arr /= (aM - am)
  return arr

def plotIndividualAnomaly(idx, centre):
  s,e = givePeriod(anomalies, idx)
  a = Normalise(np.array(retailP[centre][s:e].tolist()))
  plt.plot(a)
  plt.show()

def plotClassAvg(clss, lb, cl, mode = 'in'):
  '''
  This function will help you to plot the function
  '''
  avg = np.zeros(shape=(43,))
  count = 0
  for i in range(0, len(anomalies)):
    check = False
    if mode == 'in':
      check = (clss == anomalies[2][i])
    else:
      check = (clss in anomalies[2][i])
    if check:
      count += 1
      s,e = givePeriod(anomalies, i+1)
      a = Normalise(np.array(retailP[2][s:e].tolist()))
      avg += a
  print( count)
  avg /= count
  plt.plot(avg, color=cl, label=lb)
  plt.legend()

def RMSError(clss, mode = 'in'):
  '''
  Calculate the RMS Error
  '''
  avg = np.zeros(shape=(43,))
  count = 0
  for i in range(0, len(anomalies)):
    check = False
    if mode == 'in':
      check = (clss == anomalies[2][i])
    else:
      check = (clss in anomalies[2][i])
    if check:
      count += 1
      s,e = givePeriod(anomalies, i+1)
      a = Normalise(np.array(retailP[2][s:e].tolist()))
      avg += a
  avg /= count
  RMS = []
  for i in range(0, len(anomalies)):
    check = False
    if mode == 'in':
      check = (clss == anomalies[2][i])
    else:
      check = (clss in anomalies[2][i])
    if check:
      s,e = givePeriod(anomalies, i+1)
      b = Normalise(np.array(retailP[2][s:e].tolist()))
      RMS.append(np.sqrt(((avg - b) ** 2).mean(axis=None)))
  return (np.array(RMS)).mean()


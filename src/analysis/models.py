import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from src.constants import CONSTANTS
from src.reading.reading import retailP, mandiP, mandiA, retailPM, mandiPM, mandiAM
from src.analysis import givePeriod, Normalise

''' Read anomalies '''
''' Demo Anomalies for Lucknow Centre, Add others if you want from Anomaly Data'''
anomalies = pd.read_csv('anomalies.csv', header=None)
anomalies[2] = anomalies.apply(lambda row: row[2].split(), axis=1)

''' Create the feature set for anomalies '''
''' Can Also Use Statistical Features Here '''
centreIdx = 2
X = np.zeros(shape=(len(anomalies), 43))
for i in range(0, len(anomalies)):
  s,e = givePeriod(anomalies, i+1)
  p = Normalise(np.array(retailP[centreIdx][s:e].tolist()))   # ChangeRetailAccordingly
  for k in range(0 , len(p)):
    X[i][k] = p[k]

''' Create Label Set from the Anomaly DataSet '''
''' Example Anomaly Set for Lucknow '''
y = [3,0,0,3,3,3,5,1,2,0,4,5,5,2,3,3,4,4,1,2,1,6,1,3,6,4,6,6,3,3]
# model = RandomForestClassifier()
model = SVC(kernel='linear', C = 10)

''' Split Into Test & Train Sets '''
train = (int)0.80 * len(y)
model.fit(X[:train], y[:train])

''' Leave One Out for SVM C Optimisation '''
# clf = SVC(kernel='linear', C=1)
# scores = cross_val_score(clf, X[:train], y[:train], cv=train - 1)
# print scores

''' Get Prediction & Accuracy '''
model.predict(X[:test])

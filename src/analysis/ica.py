#!/usr/bin/env python3
'''
Finding the Anomalies using ICA
'''

from datetime import datetime
import pandas as pd
import numpy as np
from sklearn.decomposition import FastICA
from src.constants import CONSTANTS
from src.reading.reading import retailP, mandiP, mandiA, retailPM, mandiPM, mandiAM

def _IndependentComponents(seriesInp, n):
  if(len(seriesInp) != DAYS):
    print('Series not of correct dimension')
    return

  ica = FastICA(n_components=n)
  _S_ = ica.fit_transform(seriesInp)
  _A_ = ica.mixing_
  return (_S_, _A_)

def _ReconstructedSeries(seriesInp, n):
  (S_, A_) = _IndependentComponents(seriesInp, n)
  series = np.dot(S_, A_.T)
  return series

def _Residuals(seriesInp, n):
  (S_, A_) = _IndependentComponents(seriesInp, n)
  residuals = _ReconstructedSeries(seriesInp, n)

  for i in range(0, len(seriesInp)):
    for j in range(0, len(seriesInp.T)):
      residuals[i][j] -= seriesInp[i][j]

  return residuals

def _CreateWindow(seriesInp, alpha, days):
  '''
  Creates a window of error around the original
  series.
  alpha-> %age allowed error
  days-> for moving average window
  '''
  window = np.zeros(shape=(len(seriesInp), len(seriesInp.T)))

  for i in range(0, len(seriesInp)):
    for j in range(0, len(seriesInp.T)):
      if(days == 0):
        window[i][j] = abs(seriesInp[i][j] * alpha)
      else:
        for k in range(0, days):
          if(i >= days):
            window[i][j] += seriesInp[i-k][j]
          window[i][j] /= days

  if(days != 0):
    for i in range(0, days):
      for j in range(0, len(seriesInp.T)):
        window[i][j] = window[days][j]

  return window

def _ICAAnomalies(seriesInp, alpha, n, days, index):
  '''
  Anomaly function which checks against the spikes
  obtained from the residuals

  Find the best parameters (alpha, n, days)

  Index: Given Centre
  '''
  residuals = _Residuals(seriesInp, n)
  window = _CreateWindow(seriesInp, alpha, days)

  anomalies = []
  idx = pd.date_range(CONSTANTS['STARTDATE'], CONSTANTS['ENDDATE'])
  for i in range(0, CONSTANTS['DAYS']):
    if(abs(residuals.T[index][i]) > window.T[index][i]):
      anomalies.append(idx[i])

  return anomalies

def _Filter(_icaAnomalies):
  '''
  Filter: Onions can last for 4-6 weeks
  1. Anomalies, within a span of a month away from each other
  are considered to be the same anomaly.
  2. Large anomalies are broken down into periods of 2 months
  '''
  anomalies = []
  # Point One
  i = 0
  while(i < len(_icaAnomalies)):
    start = _icaAnomalies[i]
    j = i+1
    while(j < len(_icaAnomalies) and (_icaAnomalies[j] - _icaAnomalies[i]).days < 30):
      i = j
      j += 1
    end = _icaAnomalies[j-1]
    anomalies.append((start, end))
    i += 1

  #Point Two
  i = 0
  while(i < len(anomalies)):
    start = anomalies[i][0]
    end = anomalies[i][1]
    diff = (end - start).days
    if(diff > 90):
      while(diff > 60):
        anomalies.insert(i+1, (start, start + 60))
        start += 60
        diff -= 60
        i += 1
      anomalies.insert(i+1, (start, end))
      i += 1
    elif(diff > 60):
      anomalies[i] = (start, start + int(diff/2))
      anomalies.insert(i+1, (start + int(diff/2) + 1, end))
      i += 2
    else:
      i += 1
  return anomalies

def Anomalies(seriesInp, alpha, n, days, index):
  '''
  Main function for the ica module
  '''
  anomalies = _ICAAnomalies(seriesInp, alpha, n, days, index)
  anomalies = _Filter(anomalies)

  return anomalies

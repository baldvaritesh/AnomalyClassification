#!/usr/bin/env python3
'''
Get the original data and do the appropriate pruning & interpolation required
'''

from datetime import datetime
import pandas as pd
import numpy as np
import scipy
from src.constants import CONSTANTS


'''
Read the original CSVs (Important Indices)
'''
wholeSalePA = pd.read_csv(CONSTANTS['ORIGINALMANDI'], header=None)
retailP = pd.read_csv(CONSTANTS['ORIGINALRETAIL'], header=None)

'''
Indices as constants
WholeSalePrice, WholeSaleArrival, RetailPrices
StartDate, EndDate, CentreID
'''
WP = 7
WA = 2
RP = 2
START = CONSTANTS['STARTDATE']
END = CONSTANTS['ENDDATE']
CENTREID = 1

'''
Removing:
Null entries, NAN entries, entries before 01/01/2006 & after 23/06/2015
Dropping duplicate entries too
'''
wholeSalePA = wholeSalePA[wholeSalePA[WA] != 0]
wholeSalePA = wholeSalePA[wholeSalePA[WP] != 0]
retailP = retailP[retailP[RP] != 0]

wholeSalePA = wholeSalePA[np.isfinite(wholeSalePA[WA])]
wholeSalePA = wholeSalePA[np.isfinite(wholeSalePA[WP])]
retailP = retailP[np.isfinite(retailP[RP])]

wholeSalePA = wholeSalePA[wholeSalePA[0] >= START]
wholeSalePA = wholeSalePA[wholeSalePA[0] <= END]
retailP = retailP[retailP[0] >= START]
retailP = retailP[retailP[0] <= END]

wholeSalePA = wholeSalePA.drop_duplicates(subset=[0, 1], keep='last')
retailP = retailP.drop_duplicates(subset=[0, 1], keep='last')


centres = CONSTANTS['CENTRESID']
mandis = CONSTANTS['MANDIIDS']
centreNames = CONSTANTS['CENTRENAMES']


def CreateCentreSeries(Centre, RetailPandas):
  '''
  Create Centre and Mandis
  '''
  rc = RetailPandas[RetailPandas[CENTREID] == Centre]
  rc = rc.sort([0], ascending=[True])
  rc[3] = rc.apply(lambda row: datetime.strptime(row[0], '%Y-%m-%d'), axis=1)
  rc.drop(rc.columns[[0, 1]], axis=1, inplace=True)
  rc.set_index(3, inplace=True)
  rc.index.names = [None]
  idx = pd.date_range(START, END)
  rc = rc.reindex(idx, fill_value=0)
  return rc * 100

def CreateMandiSeries(Mandi, MandiPandas):
  mc = MandiPandas[MandiPandas[1] == Mandi]
  mc = mc.sort([0], ascending=[True])
  mc[8] = mc.apply(lambda row: datetime.strptime(row[0], '%Y-%m-%d'), axis=1)
  mc.drop(mc.columns[[0, 1, 3, 4, 5, 6]], axis=1, inplace=True)
  mc.set_index(8, inplace=True)
  mc.index.names = [None]
  idx = pd.date_range(START, END)
  mc = mc.reindex(idx, fill_value=0)
  return mc

'''
Create the Time Series for Every Centre and Mandi
'''
centreSeries, mandiSeries = [], []
for i in range(0, len(centres)):
  centreSeries.append(CreateCentreSeries(centres[i], retailP))
  mandiSeries.append([])
  for j in range(0, len(mandis[i])):
    mandiSeries[i].append(CreateMandiSeries(mandis[i][j], wholeSalePA))


def RemoveNaNsFront(series, idx):
  '''
  Removes the first NaN's if they are present in the data-set
  '''
  index = 0
  while True:
    if(not np.isfinite(series[idx][index])):
      index += 1
    else:
      break
  for i in range(0, index):
    series[idx][i] = series[idx][index]
  return series

'''
Interpolate the centres and the mandis data
'''
for centre in centreSeries:
  centre[RP] = centre[RP].replace('0.0', np.NaN, regex=True)
  centre[RP] = centre[RP].interpolate(method='pchip')
  centre[RP][0] = centre[RP][1]

for mandis in mandiSeries:
  for x in mandis:
    x[WP] = x[WP].replace('0.0', np.NaN, regex=True)
    x[WP] = x[WP].interpolate(method='pchip')
    x[WA] = x[WA].replace('0.0', np.NaN, regex=True)
    x[WA] = x[WA].interpolate(method='pchip')
    x = RemoveNaNsFront(x, WP)
    x = RemoveNaNsFront(x, WA)

'''
Centre the Series: Zero Mean
'''
for centre in centreSeries:
  centreMean = centre[RP].mean()
  centre[RP] -= centreMean

for mandis in mandiSeries:
  for mandi in mandis:
    mandiPriceMean = mandi[WP].mean()
    mandi[WP] -= mandiPriceMean
    mandiArrivalMean = mandi[WA].mean()
    mandi[WA] -= mandiArrivalMean

def whiten(series):
  '''
  Whitening Function
  Formula is
    W[x x.T] = E(D^(-1/2))E.T
  Here x: is the observed series
  Read here more:
  https://www.cs.helsinki.fi/u/ahyvarin/papers/NN00new.pdf
  '''
  EigenValues, EigenVectors = np.linalg.eig(series.cov())
  D = [[0.0 for i in range(0, len(EigenValues))] for j in range(0, len(EigenValues))]
  for i in range(0, len(EigenValues)):
    D[i][i] = EigenValues[i]
  DInverse = np.linalg.matrix_power(D, -1)
  DInverseSqRoot = scipy.linalg.sqrtm(D)
  V = np.dot(np.dot(EigenVectors, DInverseSqRoot), EigenVectors.T)
  series = series.apply(lambda row: np.dot(V, row.T).T, axis=1)
  return series

'''
Create corresponding dataFrames
'''
centreDF = pd.DataFrame()
for i in range(0, len(centreSeries)):
  centreDF[i] = centreSeries[i][RP]

mandiDFP = []
mandiDFA = []
for i in range(0, len(mandiSeries)):
  mandiDFP.append(pd.DataFrame())
  mandiDFA.append(pd.DataFrame())
  for j in range(0, len(mandiSeries[i])):
    mandiDFP[i][j] = mandiSeries[i][j][WP]
    mandiDFA[i][j] = mandiSeries[i][j][WA]

'''
Whiten all the data
'''
centreDF = whiten(centreDF)
for i in range(0, len(mandiSeries)):
  mandiDFP[i] = whiten(mandiDFP[i])
  mandiDFA[i] = whiten(mandiDFA[i])

'''
Create suitable format for mandis to save in single file
'''
mandiP = pd.DataFrame()
mandiA = pd.DataFrame()
for i in range(0, len(mandiSeries)):
  for j in range(0, len(mandiSeries[i])):
    mandiP[(i, j)] = mandiDFP[i][j]
    mandiA[(i, j)] = mandiDFA[i][j]

'''
Save this data
'''
centreDF.to_csv(CONSTANTS['RETAILPRICES'], header=None, encoding='utf-8')
mandiP.to_csv(CONSTANTS['MANDIPRICES'], header=None, encoding='utf-8')
mandiA.to_csv(CONSTANTS['MANDIARRIVALS'], header=None, encoding='utf-8')

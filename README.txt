README:

1. To run scripts of this project you need to build a python 3 virtual environment
2. Python2 and pip must be on your system
3. Use pip to install pip3 ($ sudo -H pip install pip3)
   This would automatically install python3 as well
4. Install virtualenv ($ sudo -H pip3 install virtualenv)
5. Use the command ($ virtualenv location/of/your/project)
6. Go to the location/of/your/project
7. Type ($ source activate)
8. This would now take you into the virtual environment, to come out use ($ deactivate)
9. Install the packages statsmodels, numpy, scipy, pandas, sklearn
10. For better usage and access install ipython in the virtual environment too.
11. Also add the current folder to the python execution path
12. Use the document strings provided with each function to get the idea of the working
